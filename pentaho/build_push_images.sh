#!/bin/bash

set -e

if [ -z "$1" ]
  then
    echo "ERROR: No version number supplied" 1>&2
    exit 1
fi

VERSION=$1

VERSION=$1 docker-compose build

docker push 678638262724.dkr.ecr.eu-central-1.amazonaws.com/pdi:$VERSION
