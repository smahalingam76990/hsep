#!/bin/bash

set -e

if [ -z "$1" ]
  then
    echo "ERROR: No version number supplied" 1>&2
    exit 1
fi

ver=$1

REGISTRY=678638262724.dkr.ecr.eu-central-1.amazonaws.com

docker-compose build

docker tag -f elk_elasticsearch ${REGISTRY}/elasticsearch:$ver
docker tag -f elk_elasticsearch ${REGISTRY}/elasticsearch:latest
docker tag -f elk_logstash ${REGISTRY}/logstash:$ver
docker tag -f elk_logstash ${REGISTRY}/logstash:latest
docker tag -f elk_kibana ${REGISTRY}/kibana:$ver
docker tag -f elk_kibana ${REGISTRY}/kibana:latest

docker push ${REGISTRY}/elasticsearch:$ver
docker push ${REGISTRY}/elasticsearch:latest
docker push ${REGISTRY}/logstash:$ver
docker push ${REGISTRY}/logstash:latest
docker push ${REGISTRY}/kibana:$ver
docker push ${REGISTRY}/kibana:latest
