## Documentation
To get the Infrastructure documentation, from the root of the repo:
* Install the requirements `pip install -r requirements.txt`
* `cd docs`
* Use make and then the file type to make documentation e.g. `make html` (To see a list of all options, just use `make`)
* The documentation can then be found in `./docs/build/`

### Using aws-fuzzy
To use aws-fuzzy for simple SSH to AWS machines, you'll need to do the following:
* Install the requirements as above
* `pip install aws-fuzzy-finder`
* Set up your aws-cli as follows:
    - `aws configure`
    - Add your access key and secret key from Amazon IAM
* You must have had your public keys added to the machine you would like to SSH as authorized keys
    - Add your username to the `usernames` var as used by the appropriate server's playbook
    - Add your public key to a file in `./ansible/user_keys/`
* To your `~/.bashrc` append the following two lines, to ensure you are using the correct username and that you are forwarding your agent
    - `export AWS_FUZZ_USER="<remote_username>"`
    - `export AWS_FUZZ_SSH_COMMAND_TEMPLATE="ssh -A {user}@{host}"`
* Restart your terminal or run `source ~/.bashrc`
* Now run `aws-fuzzy` and pick one of the available machines