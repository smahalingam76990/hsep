#!/usr/bin/env python
import json
import os
import urllib
import urllib2
import sys

url = 'http://{}/simulation/'.format(sys.argv[1])

data = urllib.urlencode({'ct': eval(os.environ.get('CT').capitalize()),
                         'sbm': eval(os.environ.get('SBM').capitalize()),
                         'tbm': eval(os.environ.get('TBM').capitalize()),
                         'daha': eval(os.environ.get('DAHA').capitalize()),
                         'iqa': eval(os.environ.get('IQA').capitalize()),
                         'day': eval(os.environ.get('DAY').capitalize()),
                         'start_date': os.environ.get('START_DATE'),
                         'end_date': os.environ.get('END_DATE'),
                         'tenant': os.environ.get('TENANT'),
                         'version': os.environ.get('VERSION'),
                         'build_number': os.environ.get('BUILD_NUMBER')})

content = urllib2.urlopen(url=url, data=data).read()
print(json.loads(content)['id'])
