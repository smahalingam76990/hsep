#!/usr/bin/env python
import os
import traceback
import urllib2

workspace = os.getenv('WORKSPACE')
simulation_id = os.getenv('SIMULATION_ID')
data_service_ip = os.getenv('DATA_SERVICE_IP')

required_env_vars = [workspace, simulation_id, data_service_ip]
if None in required_env_vars:
    raise Exception('Missing required environment variable. Received the following values {}'.format(required_env_vars))


reports = (
    'exceptions',
    'performances',
    'validation_results',
    'financial_summary'
)

base_url = 'http://{}/simulation/{}/{{}}.html'.format(data_service_ip, simulation_id)

simulation_reports_path = "{}/simulation_reports/".format(workspace)
if not os.path.exists(simulation_reports_path):
    os.makedirs(simulation_reports_path)

for report in reports:
    try:
        report_url = base_url.format(report)
        content = urllib2.urlopen(url=report_url).read()
        with open("{}{}.html".format(simulation_reports_path, report), "w") as html_file:
            html_file.write(content)
    except Exception:
        with open("{}{}_error.txt".format(simulation_reports_path, report), "w") as error_file:
            traceback.print_exc(file=error_file)
