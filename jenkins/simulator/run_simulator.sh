#!/bin/bash

set -e
set -x

: "${WORKSPACE:?Need to set WORKSPACE}"
: "${START_DATE:?Need to set START_DATE}"
: "${END_DATE:?Need to set END_DATE}"
: "${TENANT:?Need to set TENANT}"
: "${VERSION:?Need to set VERSION}"
: "${BUILD_NUMBER:?Need to set BUILD_NUMBER}"
: "${INPUT_DATA_SERVICE:?Need to set INPUT_DATA_SERVICE}"

export SBM=${SBM:-"True"}
export TBM=${TBM:-"True"}
export DAHA=${DAHA:-"True"}
export IQA=${IQA:-"True"}
export CT=${CT:-"True"}
export DAY=${DAY:-"True"}
export ANALYSER=${ANALYSER:-"True"}
export CT_OFFER_ALL=${CT_OFFER_ALL:-"False"}
export IQA_UNLIMITED=${IQA_UNLIMITED:-"False"}

export COMPOSE_HTTP_TIMEOUT=120

dc_file="${WORKSPACE}/hsep/docker/docker-compose.yml"

docker-compose -f "$dc_file" -p hsep kill
docker-compose -f "$dc_file" -p hsep rm --force

if [ ! -n "${DATA_SERVICE_IP}" ]; then
    docker-compose -f "$dc_file" -p hsep up --build -d data_service
fi
docker-compose -f "$dc_file" -p hsep up --build -d rest_api market_simulator
docker-compose -f "$dc_file" -p hsep build data_dumper data_loader simulator

running_locally=0
if [ -z "${DATA_SERVICE_IP}" ]; then
    running_locally=1
    DATA_SERVICE_IP=$(docker inspect --format="{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" hsep_data_service_1)
    while ! nc -w 1 "${DATA_SERVICE_IP}" "80" 2>/dev/null
    do
      echo -n .
      sleep 1
    done
    echo "data service ready"
fi

echo "Dumping data for ${TENANT}"
docker-compose -f "$dc_file" -p hsep run -e TENANT_ID="${TENANT}" -e VERSION="${VERSION}" -e DATA_SERVICE_IP="${INPUT_DATA_SERVICE}" data_dumper

echo "Loading ${TENANT_ID} with data_loader"
docker-compose -f "$dc_file" -p hsep run -e TENANT_ID="${TENANT}" data_loader


if [ -e "${WORKSPACE}/SimulationData/sql/hsep_common.sql" ]; then
    echo "Loading common with data_loader"
    docker-compose -f "$dc_file" -p hsep run -e TENANT_ID="common" data_loader
else
    echo "Dumping data for common"
    docker-compose -f "$dc_file" -p hsep run -e TENANT_ID="common" -e DATA_SERVICE_IP="${INPUT_DATA_SERVICE}" data_dumper

    echo "Loading common with data_loader"
    docker-compose -f "$dc_file" -p hsep run -e TENANT_ID="common" data_loader
fi


# If the variable does not have any value, then is the nightly build
if [ -z "$SIMULATION_ID" ]; then
    SIMULATION_ID=$(${WORKSPACE}/hsep/jenkins/simulator/get_simulation_id.py ${DATA_SERVICE_IP})
fi

echo ${SIMULATION_ID}
echo "SIMULATION_ID=${SIMULATION_ID}" > "${WORKSPACE}/build.properties"
echo "DATA_SERVICE_IP=${DATA_SERVICE_IP}" >> "${WORKSPACE}/build.properties"

args="-e DATA_SERVICE_IP=${DATA_SERVICE_IP} -e TENANT=${TENANT} -e SIMULATION_ID=${SIMULATION_ID} -e VERSION=${VERSION} -e BUILD_NUMBER=${BUILD_NUMBER}"
market_flags="-e SBM=${SBM} -e TBM=${TBM} -e DAHA=${DAHA} -e IQA=${IQA} -e CT=${CT} -e DAY=${DAY} -e ANALYSER=${ANALYSER} -e IQA_UNLIMITED=${IQA_UNLIMITED} -e CT_OFFER_ALL=${CT_OFFER_ALL}"

docker-compose -f ${dc_file} -p hsep run ${BACKGROUND_FLAG} ${args} ${market_flags} simulator "${START_DATE}" "${END_DATE}" "${TENANT}" "${SIMULATION_ID}"
