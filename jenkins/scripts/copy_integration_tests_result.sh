#!/bin/bash

set -x

cd $WORKSPACE

echo "Copying the Behave test report"
docker cp hsep_integration_tests_run_1:/src/test_report "$WORKSPACE"/
