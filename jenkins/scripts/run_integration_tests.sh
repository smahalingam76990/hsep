#!/bin/bash

set -e
set -x

cd "$WORKSPACE/docker"

echo "Running tests"
docker-compose run integration_tests
echo "DESTROYING EVERYTHING!!"
docker-compose kill

cd "$WORKSPACE"
