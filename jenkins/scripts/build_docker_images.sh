#!/bin/bash

set -e
set -x

make clean || true
make build
make build_pentaho
