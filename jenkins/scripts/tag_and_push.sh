#!/bin/bash

set -e
set -x

cd $WORKSPACE

registry=678638262724.dkr.ecr.eu-central-1.amazonaws.com

echo "Pushing for ${registry}"

for repo in hsep_rest_api hsep_listener hsep_worker hsep_syslog hsep_alembic_tenant hsep_alembic_common hsep_pentaho_integration hsep_pentaho_db
do
    docker tag ${repo} ${registry}/${repo}:$IMAGE_VERSION
    docker push ${registry}/${repo}:$IMAGE_VERSION
done
