#!/bin/bash

set -e
set -x

cd "$WORKSPACE/docker"

docker-compose up -d rabbitmq
docker-compose up -d rest_api
docker-compose up -d internal_rest_api

echo "Running tests"
docker-compose run unit_tests

docker cp hsep_unit_tests_run_1:/src/cover "$WORKSPACE"/
docker cp hsep_unit_tests_run_1:/src/nosetests.xml "$WORKSPACE"/

echo "Running common migrations"
docker-compose run alembic_common
echo "Running tenant migrations"
docker-compose run alembic_tenant

echo "Running semi-integration tests"
docker-compose run semi_integration_tests

echo "Running integration tests"
docker-compose run integration_tests

echo "DESTROYING EVERYTHING!!"
docker-compose kill

cd "$WORKSPACE"
