#!/bin/bash

set -x

cd $WORKSPACE

registry=678638262724.dkr.ecr.eu-central-1.amazonaws.com

# Cleanup images that were tagged for registry
for repo in hsep_rest_api hsep_listener hsep_worker hsep_syslog hsep_alembic_tenant hsep_alembic_common hsep_pentaho_integration hsep_pentaho_db
do
    docker rmi -f ${registry}/${repo}:$IMAGE_VERSION
    docker rmi -f ${repo}
done

# Cleanup images that are not tagged for registry
docker rmi -f hsep_integration_tests hsep_unit_tests hsep_autogrid_consumer hsep_market_simulator data_loader

# cleanup old mounted volumes
docker volume rm $(docker volume ls -qf dangling=true) || true

# cleanup old images
docker rmi -f $(docker images -qf dangling=true) || true
