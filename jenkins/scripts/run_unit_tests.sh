#!/bin/bash

set -e
set -x

echo "Running tests"
docker-compose -f docker/docker-compose.yml run unit_tests

docker cp hsep_unit_tests_run_1:/src/cover "$WORKSPACE"/
docker cp hsep_unit_tests_run_1:/src/coverage.xml "$WORKSPACE"/
docker cp hsep_unit_tests_run_1:/src/nosetests.html "$WORKSPACE"/

echo "DESTROYING EVERYTHING!!"
docker-compose kill

cd "$WORKSPACE"
