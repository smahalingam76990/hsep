FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

COPY pip.conf /etc/pip.conf

COPY utils/requirements.txt /src/utils/
COPY integration/requirements.txt /src/integration/
COPY workers/requirements.txt /src/workers/
COPY pentaho_integration/requirements.txt /src/pentaho_integration/
COPY market_simulator/requirements.txt /src/market_simulator/
COPY simulator/requirements.txt /src/simulator/
COPY algorithms/requirements.txt /src/algorithms/

RUN pip install -r /src/utils/requirements.txt
RUN pip install -r /src/repositories/requirements.txt
RUN pip install -r /src/handlers/requirements.txt
RUN pip install -r /src/integration/requirements.txt
RUN pip install -r /src/workers/requirements.txt
RUN pip install -r /src/pentaho_integration/requirements.txt
RUN pip install -r /src/market_simulator/requirements.txt
RUN pip install -r /src/simulator/requirements.txt
RUN pip install -r /src/algorithms/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD repositories /src/repositories
RUN pip install -e repositories

ADD handlers /src/handlers
RUN pip install -e handlers

ADD workers /src/workers
RUN pip install -e /src/workers

ADD pentaho_integration /src/pentaho_integration
RUN pip install -e /src/pentaho_integration

ADD market_simulator /src/market_simulator/
RUN pip install -e /src/market_simulator/

ADD simulator /src/simulator/
RUN pip install -e /src/simulator/

ADD algorithms /src/algorithms/
RUN pip install -e /src/algorithms/

ADD integration/tests /src/integration/tests
ADD integration/bdd /src/integration/bdd
ADD integration/load_data_from_csv_to_postgres.py /src/integration

ADD docker/integration_tests_entrypoint.sh /entrypoint.sh

COPY test_data /src/test_data

ENTRYPOINT ["bash", "/entrypoint.sh"]
