FROM postgres:9.5.2

COPY pentaho_integration/db_schema.sql /db_schema.sql
COPY docker/pentaho_db_entrypoint.sh /docker-entrypoint-initdb.d/pentaho_db_entrypoint.sh
