FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

RUN apt-get install -y wget
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main 9.5" >> /etc/apt/sources.list.d/pgdg.list
RUN wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
RUN apt-get update && apt-get install -y libpq-dev postgresql-client --fix-missing

COPY pip.conf /etc/pip.conf

COPY utils/requirements.txt /src/utils/
COPY integration/requirements.txt /src/integration/
COPY repositories/requirements.txt /src/repositories/

RUN pip install -r /src/utils/requirements.txt
RUN pip install -r /src/repositories/requirements.txt
RUN pip install -r /src/integration/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD repositories /src/repositories
RUN pip install -e repositories

ADD simulator/simulator /src/integration/simulator
ADD simulator/service_clients /src/integration/service_clients
ADD integration/load_data_from_csv_to_postgres.py /src/integration/

RUN mkdir /input
RUN mkdir /output

ADD docker/data_dump_entrypoint.sh /entrypoint.sh

ENTRYPOINT ["bash", "/entrypoint.sh"]
