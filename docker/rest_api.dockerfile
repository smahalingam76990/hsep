FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

COPY pip.conf /etc/pip.conf

RUN mkdir /src/workers

COPY utils/requirements.txt /src/utils/
COPY algorithms/requirements.txt /src/algorithms/
COPY handlers/requirements.txt /src/handlers/
COPY repositories/requirements.txt /src/repositories/
COPY rest_api/requirements.txt /src/rest_api/
COPY workers/requirements.txt /src/workers/
COPY autogrid_integration/requirements.txt /src/autogrid_integration/

RUN pip install -r utils/requirements.txt
RUN pip install -r algorithms/requirements.txt
RUN pip install -r handlers/requirements.txt
RUN pip install -r repositories/requirements.txt
RUN pip install -r rest_api/requirements.txt
RUN pip install -r workers/requirements.txt
RUN pip install -r autogrid_integration/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD algorithms /src/algorithms
RUN pip install -e algorithms

ADD handlers /src/handlers
RUN pip install -e handlers

ADD repositories /src/repositories
RUN pip install -e repositories

ADD rest_api /src/rest_api
RUN pip install -e rest_api

ADD workers /src/workers
RUN pip install -e workers

ADD autogrid_integration /src/autogrid_integration
RUN pip install -e autogrid_integration

ADD docker/rest_api_entrypoint.sh /entrypoint.sh

ENTRYPOINT ["bash", "/entrypoint.sh"]
