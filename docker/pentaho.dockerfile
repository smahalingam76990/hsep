FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/pdi:6.1-1

USER root

ENV PYTHONUNBUFFERED 1
RUN apt-get update --fix-missing && \
    apt-get upgrade -y && \
    apt-get install -y build-essential \
    python python-pip python-dev \
    libffi-dev libssl-dev \
    libpq-dev postgresql-client \
    wget net-tools && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/*

RUN pip install -U pip
RUN pip install -U requests["security"]==2.11.1

COPY utils/requirements.txt utils/
RUN pip install -r utils/requirements.txt

COPY pentaho_integration/requirements.txt pentaho_integration/
RUN pip install -r pentaho_integration/requirements.txt

COPY pentaho_integration pentaho_integration
COPY utils utils

RUN pip install -e pentaho_integration
RUN pip install -e utils

# Create directory for downloading files and temporary files
RUN mkdir -p cached_files/
