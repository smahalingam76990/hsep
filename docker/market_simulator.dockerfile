FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

COPY pip.conf /etc/pip.conf

COPY utils/requirements.txt /src/utils/
COPY market_simulator/requirements.txt /src/market_simulator/
COPY algorithms/requirements.txt /src/algorithms/

RUN pip install -r utils/requirements.txt
RUN pip install -r market_simulator/requirements.txt
RUN pip install -r algorithms/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD market_simulator /src/market_simulator
RUN pip install -e market_simulator

ADD algorithms /src/algorithms
RUN pip install -e algorithms

ADD test_data /src/test_data

ADD docker/market_simulator_entrypoint.sh /entrypoint.sh

ENTRYPOINT ["bash", "/entrypoint.sh"]
