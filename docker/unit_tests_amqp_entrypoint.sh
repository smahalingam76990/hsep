#!/bin/bash

set -e
set -x

nosetests -v /src/listener -a 'semi_integration'
nosetests -v /src/autogrid_integration -a 'semi_integration'
