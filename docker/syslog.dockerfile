FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/rsyslog:8.23.0

COPY docker/config/rsyslog.conf /etc/rsyslog.conf
COPY docker/config/syslog-hsep.conf /etc/rsyslog.d/60-hsep.conf
COPY docker/syslog_entrypoint.sh /entrypoint.sh

VOLUME /dev

ENTRYPOINT ["/entrypoint.sh"]
