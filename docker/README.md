### To get started:

  * Install Virtualbox
  * Install docker-machine
  * `docker-machine create --driver virtualbox dev`
  * `docker-machine start dev` (if dev already created)
  * `eval $(docker-machine env dev)`
  * `sh jenkins/scripts/build_docker_images.sh`

### Each time you make changes to the code and want to see the results:
  * `sh jenkins/scripts/build_docker_images.sh`
  * `sh jenkins/scripts/run_tests.sh`

### To run the rest_api:
  * `docker-compose up rest_api`
  * Retrieve the ip address of the docker container using `docker-machine env dev`
  * Access the rest api in the browser via _ip_:8090/

### Access credentials:
  * Open _ip_:8090/admin
  * Login with super user credentials (see rest_api_entrypoint.sh)
  * Under 'DJANGO OAUTH TOOLKIT' access Applications
  * Choose the appropriate Application for the required tenant or 'ADD APPLICATION +'
  * Retrieve client_id from application
  * POST _ip_:8090/o/token/?grant_type=password using the the client_id as well as username and password for tenant
  * Use access_token in the header for your GET/POST requests (key='Authorization' value='bearer <access_token>'
 
### Migrations:
 If you have made changes to the databases you wish to migrate:
 *  `cd docker`
 *  `docker-compose run alembic_tenant revision --autogenerate -m "<message here>"`
 *  Alter the revision python file to remove comments and fix any errors in migration script
 * `docker-compose run alembic_tenant`
 
### Putting Data into Database:
 To load data from sql dump:
 * `export PGPASSWORD=hsep; export PGUSER=hsep; export PGHOST=<_ip_>;`
 * `psql hsep_common -f <path_to_SimulationData>/SimulationData/sql/hsep_common.sql`
 * `psql hsep_<tenant> -f <path_to_SimulationData>/SimulationData/sql/hsep_<tenant>.sql`
 To load data from csv:
 * `export POSTGRES_USER=hsep; export POSTGRES_PASSWORD=hsep; export POSTGRES_HOST=<_ip_>; export TENANT_ID=<tenant>; `
 * `python integration/load_data_from_csv_to_postgres <path_to_csv_directory>`
 
### Run kibana locally
 To start kibana (that will bring up elasticsearch as well):
 * `docker-compose up -d kibana`
 To start logstash (that will bring up elasticsearch as well):
 * `docker-compose up -d logstash`
 