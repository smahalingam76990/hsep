#!/bin/bash

set -e

uwsgi --socket 0.0.0.0:80 --protocol=http -w market_simulator:wsgi_app --processes 4 --threads 4 --master
