#!/bin/bash
set -e

export PGUSER="$POSTGRES_USER"
export PGPASSWORD="$POSTGRES_PASSWORD"

for tenant in $TENANTS
do
    echo "creating database for ${tenant}"
    createdb "hsep_${tenant}"
done

for extra_db in $POSTGRES_EXTRA_DBS
do
    echo "creating ${extra_db}"
    createdb $extra_db
done
