#!/bin/bash
set -e

export PGUSER="$POSTGRES_USER"
export PGPASSWORD="$POSTGRES_PASSWORD"
export POSTGRES_DB="$POSTGRES_DB"

psql $POSTGRES_DB -f /db_schema.sql
