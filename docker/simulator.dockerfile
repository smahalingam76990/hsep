FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

RUN apt-get update --fix-missing && \
    apt-get install -y zip

COPY pip.conf /etc/pip.conf

RUN mkdir /src/simulator/

COPY utils/requirements.txt /src/utils/
COPY simulator/requirements.txt /src/simulator/

RUN pip install -r /src/simulator/requirements.txt
RUN pip install -r /src/utils/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD simulator /src/simulator
RUN pip install -e /src/simulator

RUN mkdir /src/output/

ADD docker/simulator_entrypoint.sh /entrypoint.sh

ENTRYPOINT ["bash", "/entrypoint.sh"]
