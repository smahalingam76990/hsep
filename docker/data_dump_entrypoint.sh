#!/bin/bash

set -e

: "${TENANT_ID:?Need to set TENANT_ID}"

export PGHOST=${SQL_HOST}
export PGPORT=${SQL_TCP_PORT}
export PGUSER=${SQL_USER}
export PGPASSWORD=${SQL_PASSWORD}

echo -n "waiting for TCP connection to $PGHOST:$PGPORT..."

while ! nc -w 1 "$PGHOST" "$PGPORT" 2>/dev/null
do
  echo -n .
  sleep 1
done
echo 'ok'

sleep 3


if [[ -n $(psql -Atqc "\list hsep_${TENANT_ID}" postgres) ]]; then
    echo "Dropping db: ${TENANT_ID}"
    dropdb "hsep_${TENANT_ID}"
fi

# Specify a local root folder if you don't want to use data service as --local_dir="/input"
if [ "$TENANT_ID" = "common" ]; then
    python /src/integration/load_data_from_csv_to_postgres.py --no-tenant --local_dir=/input "$@"
else
    python /src/integration/load_data_from_csv_to_postgres.py --no-common --local_dir=/input "$@"
fi

pg_dump --file="/output/hsep_${TENANT_ID}.sql" --clean --if-exists --no-owner --dbname="hsep_${TENANT_ID}"
