#!/bin/bash

set -e
set -x

chown syslog:syslog /var/log/hsep

rsyslogd -n
