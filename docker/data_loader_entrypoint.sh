#!/bin/bash

set -e

: "${TENANT_ID:?Need to set TENANT_ID}"

export PGHOST=${SQL_HOST}
export PGPORT=${SQL_TCP_PORT}
export PGUSER=${SQL_USER}
export PGPASSWORD=${SQL_PASSWORD}

echo -n "waiting for TCP connection to $PGHOST:$PGPORT..."

while ! nc -w 1 "$PGHOST" "$PGPORT" 2>/dev/null
do
  echo -n .
  sleep 1
done

echo 'ok'

if [[ -z $(psql -Atqc "\list hsep_${TENANT_ID}" postgres) ]]; then
    psql -v ON_ERROR_STOP=1 -d hsep_auth_db --no-password -c "CREATE DATABASE hsep_${TENANT_ID};"
else
    echo "Database for ${TENANT_ID} already exists"
fi

psql "hsep_${TENANT_ID}" -f "/input/hsep_${TENANT_ID}.sql"
