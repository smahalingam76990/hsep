#!/bin/bash

set -e
# The script is defined as an entrypoint when installing the simulator package, look at the setup.py
simulator "$@"
