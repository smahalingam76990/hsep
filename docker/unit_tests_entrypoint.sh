#!/bin/bash

set -e
set -x

export packages="hsep_utils listener algorithms market_simulator handlers repositories analyser simulator workers"

export package_string
package_string=$(echo "${packages}" | python -c 'import sys; print ",".join(sys.stdin.read().split())')

# We don't need --cover-xml --with-html but they seem to fix te logging
nosetests -v /src/* --with-coverage --cover-package="${package_string}" --cover-inclusive --cover-html --with-xunit --cover-xml --with-html
nosetests -v /rest_api
