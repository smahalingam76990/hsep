#!/bin/bash

set -e

if [[ "$@" == *--celerybeat* ]]; then
    celery -A hsep_worker.celerybeat beat -S hsep_worker.scheduler.tenant_celery_beat_scheduler.TenantPersistentScheduler --max-interval 60
elif [[ "$@" == *--flower* ]]; then
    SYSLOG_FACILITY=local7 flower -A hsep_worker.worker --port=5555
else
    celery -A hsep_worker.worker worker -c 1 --loglevel=INFO
fi
