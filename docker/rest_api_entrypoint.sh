#!/bin/bash

set -e

: "${POSTGRES_HOST:?Need to set POSTGRES_HOST}"
: "${POSTGRES_TCP_PORT:?Need to set POSTGRES_TCP_PORT}"

host=${POSTGRES_HOST}
port=${POSTGRES_TCP_PORT}

echo -n "waiting for TCP connection to $host:$port..."

while ! nc -w 1 "$host" "$port" 2>/dev/null
do
  echo -n .
  sleep 1
done
echo 'ok'


if [[ "$@" == *--migrate* ]]; then
    python rest_api/manage.py migrate rest_api
    python rest_api/manage.py migrate
fi

if [[ "$@" == *--add_tenants* ]]; then
    python rest_api/manage.py create_user --username=admin --password=changeme --super=True --tenant=None
    python rest_api/manage.py add_tenants --config="/src/config/tenants.yml"
fi


if [[ "$@" == *--runserver* ]]; then
    python rest_api/manage.py runserver
else
    uwsgi --socket 0.0.0.0:80 --protocol=http -w rest_api.wsgi:application --processes 4
fi
