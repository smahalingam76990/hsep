FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

COPY pip.conf /etc/pip.conf

COPY utils/requirements.txt /src/utils/
COPY algorithms/requirements.txt /src/algorithms/
COPY handlers/requirements.txt /src/handlers/
COPY repositories/requirements.txt /src/repositories/
COPY rest_api/requirements.txt /rest_api/
COPY market_simulator/requirements.txt /src/market_simulator/
COPY listener/requirements.txt /src/listener/
COPY autogrid_integration/requirements.txt /src/autogrid_integration/
COPY pentaho_integration/requirements.txt /src/pentaho_integration/
COPY integration/requirements.txt /src/integration/
COPY workers/requirements.txt /src/workers/
COPY simulator/requirements.txt /src/simulator/

RUN pip install -r /src/utils/requirements.txt
RUN pip install -r /src/algorithms/requirements.txt
RUN pip install -r /src/handlers/requirements.txt
RUN pip install -r /src/repositories/requirements.txt
RUN pip install -r /rest_api/requirements.txt
RUN pip install -r /src/market_simulator/requirements.txt
RUN pip install -r /src/listener/requirements.txt
RUN pip install -r /src/autogrid_integration/requirements.txt
RUN pip install -r /src/pentaho_integration/requirements.txt
RUN pip install -r /src/workers/requirements.txt
RUN pip install -r /src/simulator/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD algorithms /src/algorithms
RUN pip install -e /src/algorithms

ADD handlers /src/handlers
RUN pip install -e /src/handlers

ADD repositories /src/repositories
RUN pip install -e /src/repositories

ADD rest_api /rest_api
RUN pip install -e /rest_api

ADD market_simulator /src/market_simulator
RUN pip install -e /src/market_simulator

ADD listener /src/listener
RUN pip install -e /src/listener

ADD autogrid_integration /src/autogrid_integration
RUN pip install -e /src/autogrid_integration

ADD pentaho_integration /src/pentaho_integration
RUN pip install -e /src/pentaho_integration

ADD workers /src/workers
RUN pip install -e /src/workers

ADD simulator /src/simulator
RUN pip install -e /src/simulator

ADD test_requirements.txt /test_requirements.txt
RUN pip install -r /test_requirements.txt

ADD docker/unit_tests_entrypoint.sh /unit_tests_entrypoint.sh
ADD docker/unit_tests_amqp_entrypoint.sh /unit_tests_amqp_entrypoint.sh

COPY test_data /src/test_data

ENTRYPOINT ["bash", "/unit_tests_entrypoint.sh"]
