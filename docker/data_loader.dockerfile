FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

RUN apt-get install -y wget
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main 9.5" >> /etc/apt/sources.list.d/pgdg.list
RUN wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
RUN apt-get update && apt-get install -y libpq-dev postgresql-client --fix-missing

COPY pip.conf /etc/pip.conf

ADD docker/data_loader_entrypoint.sh /entrypoint.sh

ENTRYPOINT ["bash", "/entrypoint.sh"]
