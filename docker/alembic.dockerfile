FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

RUN mkdir -p /src
WORKDIR /src

COPY pip.conf /etc/pip.conf

RUN mkdir -p /src/repositories
ADD repositories/requirements.txt /src/repositories/requirements.txt
RUN pip install -r /src/repositories/requirements.txt
RUN mkdir -p /src/utils
ADD utils/requirements.txt /src/utils/requirements.txt
RUN pip install -r /src/utils/requirements.txt

ADD repositories /src/repositories
RUN pip install -e /src/repositories

ADD utils /src/utils
RUN pip install -e /src/utils

ADD repositories/alembic_tenant /src/alembic_tenant
ADD repositories/alembic_common /src/alembic_common

ADD docker/alembic_entrypoint.sh /entrypoint.sh
WORKDIR /src/repositories

ENTRYPOINT ["bash", "/entrypoint.sh"]
