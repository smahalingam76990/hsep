FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

COPY utils/requirements.txt /src/utils/
RUN pip install -r utils/requirements.txt
COPY listener/requirements.txt /src/listener/
RUN pip install -r listener/requirements.txt
COPY autogrid_integration/requirements.txt /src/autogrid_integration/
RUN pip install -r autogrid_integration/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD autogrid_integration /src/autogrid_integration
RUN pip install -e autogrid_integration

ADD listener /src/listener
RUN pip install -e listener

WORKDIR /src/listener
