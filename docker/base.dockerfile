FROM ubuntu:14.04

ENV PYTHONUNBUFFERED 1
RUN apt-get update --fix-missing && \
    apt-get upgrade -y && \
    apt-get install -y build-essential \
    python python-pip python-dev \
    libffi-dev libssl-dev \
    libpq-dev postgresql-client \
    wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/*

RUN pip install -U pip
RUN pip install -U requests["security"]==2.11.1

RUN pip install pandas==0.18.1

RUN mkdir /src

WORKDIR /src

RUN mkdir /src/utils
RUN mkdir /src/algorithms
RUN mkdir /src/handlers
RUN mkdir /src/repositories
RUN mkdir /src/rest_api

COPY utils/requirements.txt /src/utils/
COPY algorithms/requirements.txt /src/algorithms/
COPY handlers/requirements.txt /src/handlers/
COPY repositories/requirements.txt /src/repositories/
COPY rest_api/requirements.txt /src/rest_api/

RUN pip install -r utils/requirements.txt
RUN pip install -r algorithms/requirements.txt
RUN pip install -r handlers/requirements.txt
RUN pip install -r repositories/requirements.txt
RUN pip install -r rest_api/requirements.txt

RUN mkdir /var/log/hsep/
ENV LOG_DIR /var/log/hsep/
VOLUME ["/var/log/hsep/"]
