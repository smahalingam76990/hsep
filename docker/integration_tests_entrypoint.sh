#!/bin/bash

set -e

: "${SQL_HOST:?Need to set SQL_HOST}"
: "${SQL_TCP_PORT:?Need to set SQL_TCP_PORT}"

export PGHOST=${SQL_HOST}
export PGPORT=${SQL_TCP_PORT}

echo -n "waiting for TCP connection to $PGHOST:$PGPORT..."

while ! nc -w 1 "$PGHOST" "$PGPORT" 2>/dev/null
do
  echo -n .
  sleep 1
done
echo 'ok'

behave /src/integration/bdd/features/ --capture --summary --junit --junit-directory test_report --tags ~known_bug
nosetests -v /src/integration
