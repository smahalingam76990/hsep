FROM 678638262724.dkr.ecr.eu-central-1.amazonaws.com/hsep_base:0.3

COPY utils/requirements.txt /src/utils/
COPY algorithms/requirements.txt /src/algorithms/
COPY handlers/requirements.txt /src/handlers/
COPY repositories/requirements.txt /src/repositories/

RUN pip install -r utils/requirements.txt
RUN pip install -r algorithms/requirements.txt
RUN pip install -r handlers/requirements.txt
RUN pip install -r repositories/requirements.txt

ADD utils /src/utils
RUN pip install -e utils

ADD algorithms /src/algorithms
RUN pip install -e algorithms

ADD handlers /src/handlers
RUN pip install -e handlers

ADD repositories /src/repositories
RUN pip install -e repositories


COPY workers/requirements.txt /src/workers/
RUN pip install -r workers/requirements.txt
ADD workers /src/workers
RUN pip install -e workers
WORKDIR /src

ENV C_FORCE_ROOT true

ADD docker/worker_entrypoint.sh /entrypoint.sh
ENTRYPOINT ["bash", "/entrypoint.sh"]
