#!/bin/bash

set -e
set -x

docker-compose kill
docker-compose rm --all --force
if [[ "$@" == *--dev* ]]; then
    docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build -d rest_api market_simulator
else
    docker-compose -f docker-compose.yml up --build -d rest_api market_simulator
fi

docker-compose build data_loader

export TENANT_IDS="sim2015 scenarios olaf common uat2016";
for TENANT_ID in $TENANT_IDS;
do
    echo "Loading ${TENANT_ID} with data_loader"
    docker-compose run -e TENANT_ID="$TENANT_ID" data_loader
done

docker-compose logs rest_api
