#!/bin/bash

set -e

: "${SQL_HOST:?Need to set SQL_HOST}"
: "${SQL_TCP_PORT:?Need to set SQL_TCP_PORT}"
: "${SQL_USER:?Need to set SQL_USER}"
: "${SQL_PASSWORD:?Need to set SQL_PASSWORD}"

export PGHOST=${SQL_HOST}
export PGPORT=${SQL_TCP_PORT}
export PGUSER=${SQL_USER}
export PGPASSWORD=${SQL_PASSWORD}
tenant_db=hsep_${TENANT_ID}

echo -n "waiting for TCP connection to $PGHOST:$PGPORT..."

while ! nc -w 1 "$PGHOST" "$PGPORT" 2>/dev/null
do
  echo -n .
  sleep 1
done
echo 'ok'

sleep 3

if [[ -z $(psql -Atqc "\list $tenant_db" postgres) ]]; then
    psql -v ON_ERROR_STOP=1 -d hsep_auth_db --no-password -c "CREATE DATABASE $tenant_db;"
else
    echo "Database for $tenant_db already exists"
fi

export DB_URL="postgres://$PGUSER:$PGPASSWORD@$PGHOST:$PGPORT/$tenant_db"
if [ "$TENANT_ID" = "common" ]; then
    (cd /src/alembic_common && alembic "$@")
else
    (cd /src/alembic_tenant && alembic "$@")
fi
