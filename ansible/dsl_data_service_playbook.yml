---
- include: security_groups_playbook.yml data_service_elb=yes

- name: Create RDS and EC2 instances
  hosts: local
  connection: local
  gather_facts: False
  tasks:

    - name: Fetch security groups
      ec2_group_facts:
        groups: "{{ RDS_vpc_security_groups }}"
        vpc_id: "{{ VPC_ID }}"
        region: "{{AWS_REGION}}"
      register: rds_groups
      tags: deploy
    - debug: var=rds_groups

    - name: Set up RDS instance
      rds:
        command: create
        instance_name: "{{ RDS_instance_name }}"
        db_engine: postgres
        size: "{{ RDS_size | default(105) }}"
        region: "{{ AWS_REGION }}"
        backup_retention: "{{ RDS_backup_retention | default(7) }}"
        instance_type: "{{ RDS_instance_type }}"
        vpc_security_groups: "{{rds_groups.groups | map(attribute='id') | join(',') }}"
        username: "{{ RDS_username }}"
        password: "{{ RDS_password }}"
        multi_zone: "{{ RDS_multi_zone }}"
        wait: yes
        tags: "{{ RDS_tags }}"
      register: rds
      tags: deploy
    - debug: msg="{{rds}}"

    - name: Creating DNS record for the rds endpoint
      route53:
        command: create
        zone: hflexenergy.com
        record: "{{ POSTGRES_HOST }}"
        type: CNAME
        ttl: 7200
        value: "{{ rds.instance.endpoint }}"
        overwrite: yes

    - name: Adds postgres databases
      postgresql_db:
        name: "{{ item }}"
        login_user: "{{ POSTGRES_USER }}"
        login_password: "{{ POSTGRES_PASS }}"
        login_host: "{{ rds.instance.endpoint }}"
      with_items:
        - "{{ RDS_postgres_database }}"

    - name: Create EC2 Instances for data service
      ec2:
        region: "{{ AWS_REGION }}"
        image: "{{ EC2_IMAGE }}"
        instance_type: t2.large
        key_name: "{{ EC2_SSH_KEY }}"
        group: default,Office_SSH
        user_data: "{{ ubuntu_16_04_docker_user_data }}"
        exact_count: 1
        count_tag:
          Name: "Data Service"
        instance_tags:
          Name: "Data Service"
        ebs_optimized: no
        volumes:
          - device_name: /dev/sda1
            volume_size: 64
            device_type: gp2
            delete_on_termination: yes
        wait: yes
      register: data_service_machines
      tags: deploy

    - name: Wait for SSH
      wait_for:
        host: "{{ item.public_ip }}"
        port: 22
        delay: 5
        timeout: 180
        state: started
      with_items: "{{ data_service_machines.tagged_instances }}"

    - name: Creating DNS record for data service on "{{PRIVATE_DOMAIN_ZONE}}"
      route53:
        command: create
        zone: "{{PRIVATE_DOMAIN_ZONE}}"
        private_zone: yes
        record: "dataservice.{{PRIVATE_DOMAIN_ZONE}}"
        type: CNAME
        ttl: 3600
        value: "{{data_service_machines.tagged_instances[0].private_dns_name}}"
        overwrite: yes

    - name: Add hosts to the known hosts
      known_hosts:
        name: "{{ item.public_ip }}"
        key: "{{ lookup('pipe', 'ssh-keyscan -t ecdsa-sha2-nistp256 '+item.public_ip) }}"
      with_items: "{{data_service_machines.tagged_instances}}"
      tags: deploy

    - name: Add hsep instances to host groups
      add_host:
        name: "{{ item.public_ip }}"
        groups: dsl,dsl_data_service
      with_items: "{{ data_service_machines.tagged_instances }}"
      tags: deploy

- hosts: dsl
  roles:
    - { role: docker_install, tags: [ 'docker' ] }
    - { role: aws_registry_access, tags: [ 'aws_registry_access' ] }
  tasks:
    - name: ECR login
      shell: "$(aws ecr get-login --region {{AWS_REGION}})"
      tags:
        - deploy

- hosts: dsl_data_service
  roles:
    - { role: docker_data_service, tags: [ 'dsl_data_service', 'deploy' ] }

- name: Configures a load balancer
  hosts: local
  connection: local
  gather_facts: False
  vars:
    elb_zones:
      - "{{data_service_machines.tagged_instances[0].placement}}"
    instances: "{{data_service_machines.tagged_instances}}"
  tasks:

    - name: Create elastic load balancer
      ec2_elb_lb:
        name: "data-service-elb"
        region: "{{ AWS_REGION }}"
        health_check:
          ping_protocol: http # options are http, https, ssl, tcp
          ping_port: "{{ data_service_rest_api_port | default(80) }}"
          ping_path: "/admin/login/"
          response_timeout: 5
          interval: 20
          unhealthy_threshold: 2
          healthy_threshold: 2
        security_group_names:
         - default
         - data_service_elb
        zones: "{{ elb_zones }}"
        state: present
        wait: yes
        listeners:
          - protocol: https
            load_balancer_port: 443
            instance_protocol: http
            instance_port: "{{ data_service_rest_api_port | default(80) }}"
            ssl_certificate_id: "{{AWS_hflexenergy_cert}}"
      register: elb

    - name: Attach instances on the load balancer
      ec2_elb:
        region: "{{ AWS_REGION }}"
        instance_id: "{{ item.id }}"
        ec2_elbs: "data-service-elb"
        enable_availability_zone: no
        state: present
        wait: no
      with_items: "{{ instances }}"
      when: item.state != "terminated"

    - name: Creating DNS record for the rest api
      route53:
        command: create
        zone: hflexenergy.com
        record: "{{item}}.hflexenergy.com"
        type: CNAME
        ttl: 7200
        value: "{{elb.elb.dns_name}}"
        overwrite: yes
      with_items:
        - dataservice
