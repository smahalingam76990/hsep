---
- include: security_groups_playbook.yml jenkins_elb=yes

- name: Create EC2 instances for jenkins master and slaves
  hosts: local
  connection: local
  gather_facts: False
  vars:
    slaves_list:
      - number: 1
        instance_type: t2.medium
      - number: 2
        instance_type: t2.medium
      - number: 3
        instance_type: t2.medium
      - number: 4
        instance_type: t2.medium
      - number: 5
        instance_type: t2.medium
      - number: 6
        instance_type: t2.medium
      - number: 7
        instance_type: t2.medium
        wait: yes
      - number: 10
        instance_type: c4.large
        wait: yes
  tasks:

    - name: Create EC2 Instances for jenkins master
      ec2:
        region: "{{ AWS_REGION }}"
        image: "{{ EC2_IMAGE }}"
        instance_type: "{{ EC2_INSTANCE_TYPE }}"
        key_name: "{{ EC2_SSH_KEY }}"
        group: default,Office_SSH
        user_data: "{{ ubuntu_16_04_docker_user_data }}"
        exact_count: 1
        count_tag:
          Name: "Jenkins Master"
        instance_tags:
          Name: "Jenkins Master"
        ebs_optimized: no
        volumes:
          - device_name: /dev/sda1
            volume_size: 64
            device_type: gp2
            delete_on_termination: yes
        wait: yes

    - name: Create EC2 Instances for jenkins slaves
      ec2:
        region: "{{ AWS_REGION }}"
        image: "{{ EC2_IMAGE }}"
        instance_type: "{{ item.instance_type }}"
        key_name: "{{ EC2_SSH_KEY }}"
        group: default,Office_SSH
        user_data: "{{ ubuntu_16_04_docker_user_data }}"
        exact_count: 1
        count_tag:
          Name: "Jenkins Slave {{item.number}}"
          Type: "Jenkins Slave"
        instance_tags:
          Name: "Jenkins Slave {{item.number}}"
          Type: "Jenkins Slave"
          SubDomain: "jenkins-slave-{{item.number}}"
        ebs_optimized: no
        volumes:
          - device_name: /dev/sda1
            volume_size: 64
            device_type: gp2
            delete_on_termination: yes
        wait: "{{item.wait|default('no')}}"
      with_items: "{{slaves_list}}"

    - ec2_remote_facts:
        region: "{{ AWS_REGION }}"
        filters:
          instance-state-name: running
          "tag:Name": "Jenkins Master"
      register: jenkins_master_machine

    #- debug: msg="{{jenkins_master_machine}}"

    - ec2_remote_facts:
        region: "{{ AWS_REGION }}"
        filters:
          instance-state-name: running
          "tag:Type": "Jenkins Slave"
      register: jenkins_slave_machines

    #- debug: msg="{{jenkins_slave_machines}}"

    - name: Wait for SSH
      wait_for:
        host: "{{ item.public_dns_name }}"
        port: 22
        delay: 5
        timeout: 180
        state: started
      with_items: "{{ jenkins_master_machine.instances + jenkins_slave_machines.instances }}"

    - name: Creating DNS record for each slave on hflexenergy.com
      route53:
        command: create
        zone: hflexenergy.com
        record: "{{item.tags.SubDomain}}.hflexenergy.com"
        type: CNAME
        ttl: 3600
        value: "{{ item.public_dns_name }}"
        overwrite: yes
      with_items: "{{ jenkins_slave_machines.instances }}"

    - name: Creating DNS record for each slave on "{{PRIVATE_DOMAIN_ZONE}}"
      route53:
        command: create
        zone: "{{PRIVATE_DOMAIN_ZONE}}"
        private_zone: yes
        record: "{{item.tags.SubDomain}}.{{PRIVATE_DOMAIN_ZONE}}"
        type: CNAME
        ttl: 3600
        value: "{{ item.private_dns_name }}"
        overwrite: yes
      with_items: "{{ jenkins_slave_machines.instances }}"

    - name: Add hosts to the known hosts
      known_hosts:
        name: "{{ item.public_dns_name }}"
        key: "{{ lookup('pipe', 'ssh-keyscan -t ecdsa-sha2-nistp256 '+item.public_dns_name) }}"
      with_items: "{{ jenkins_master_machine.instances + jenkins_slave_machines.instances }}"

    - name: Add jenkins master instance to host groups
      add_host:
        name: "{{ item.public_dns_name }}"
        groups: jenkins,jenkins_master
      with_items: "{{ jenkins_master_machine.instances }}"
    - name: Add jenkins slave instances to host groups
      add_host:
        name: "{{ item.public_dns_name }}"
        groups: jenkins,jenkins_slave
      with_items: "{{ jenkins_slave_machines.instances }}"

- hosts: jenkins
  roles:
    - docker_install

- hosts: jenkins_master
  roles:
    - aws_registry_access
    - docker_jenkins_master

- hosts: jenkins_slave
  roles:
    - aws_registry_access
    - jenkins_slave


- name: Configures a load balancer
  hosts: local
  connection: local
  gather_facts: False
  vars:
    elb_zones:
      - "{{jenkins_master_machine.instances[0].placement.zone}}"
    instances: "{{jenkins_master_machine.instances}}"
    elb_name: jenkins-ui-elb
    security_groups:
      - default
      - jenkins_elb_office_access
    listeners:
      - protocol: https
        load_balancer_port: 443
        instance_protocol: http
        instance_port: "{{ jenkins_master_port | default(8080) }}"
        ssl_certificate_id: "{{AWS_hflexenergy_cert}}"
    health_check:
      ping_protocol: http # options are http, https, ssl, tcp
      ping_port: "{{ jenkins_master_port | default(8080) }}"
      ping_path: "/login"
      response_timeout: 5
      interval: 20
      unhealthy_threshold: 2
      healthy_threshold: 2
    domain_zone: hflexenergy.com
    sub_domains:
      - jenkins

  roles:
    - aws_load_balancer
