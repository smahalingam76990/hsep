#!/usr/bin/python

DOCUMENTATION = '''
---
module: ecs_repositories
short_description: Manages repositories on ECS
version_added: "0.1"
description:
     - Manages repositories on ECS.
options:
  registry_id:
    description:
      - The IP:PORT or a list of IPs and ports of the swarm managers
    default: null
    required: false
  state:
    description:
      - The desired state of the repository
    required: false
    choices: ["present", "absent"]
    default: present
  name:
    description:
      - The name of the repository, use it if you want to add or remove a repository
    required: false
    default: null
extends_documentation_fragment:
    - aws
    - ec2
requirements: [ json, boto, botocore, boto3 ]
authors:
  - Ilias Kiourktsidis
'''

EXAMPLES = '''
'''

RETURN = '''
'''

try:
    import boto
    import botocore
    HAS_BOTO = True
except ImportError:
    HAS_BOTO = False

try:
    import boto3
    HAS_BOTO3 = True
except ImportError:
    HAS_BOTO3 = False

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.ec2 import boto3_conn, ec2_argument_spec, get_aws_connection_info


class EcrServiceManager:
    """Handles ECR Services"""

    def __init__(self, module, results):
        self.module = module
        self.results = results

        try:
            region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module, boto3=True)
            if not region:
                module.fail_json(msg="Region must be specified as a parameter, in EC2_REGION or AWS_REGION environment variables or in boto configuration file")
            self.ecs = boto3_conn(module, conn_type='client', resource='ecr', region=region, endpoint=ec2_url, **aws_connect_kwargs)
        except boto.exception.NoAuthHandlerFound as e:
            self.module.fail_json(msg="Can't authorize connection - %s" % str(e))

        self.registry_id = self.module.params.get('registry_id')

    def list_repos(self):
        self.results['list_of_repos'] = self.ecs.describe_repositories(registryId=self.registry_id)

    def create_repo(self, repo_name):
        try:
            self.results['create_repo_result'] = self.ecs.create_repository(repositoryName=repo_name)
        except botocore.exceptions.ClientError as exc:
            if exc.response['Error']['Code'] == 'RepositoryAlreadyExistsException':
                self.results['changed'] = False
            else:
                raise
        else:
            self.results['changed'] = True


def main():

    argument_spec = ec2_argument_spec()
    argument_spec.update(dict(
        registry_id=dict(type='str', required=False),
        name=dict(type='str', required=False),
        state=dict(type='str', default='present', choices=['present', 'absent']),
    ))

    module = AnsibleModule(argument_spec=argument_spec)

    if not HAS_BOTO:
        module.fail_json(msg='boto required for this module')

    if not HAS_BOTO3:
        module.fail_json(msg='boto3 is required.')

    results = dict(changed=False)
    service_mgr = EcrServiceManager(module, results)

    if module.params.get('name'):
        if module.params.get('state') == 'present':
            service_mgr.create_repo(module.params.get('name'))
        else:
            module.fail_json(msg='State {} not implemented yet'.format(module.params.get('state')))
    else:
        service_mgr.list_repos()

    module.exit_json(**results)


# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

if __name__ == '__main__':
    main()
