#!/usr/bin/python

#<<INCLUDE_ANSIBLE_MODULE_COMMON>>
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *
import boto.ec2

DOCUMENTATION = '''
---
module: rds_instance_facts
short_description: gets information about an rds instance by its instance name
description:
    - Look up an RDS instance by instance name and optional filters.
options:
    groups:
        description: A list of group names to search for.
        required: false
        aliases: ['name']
    group_ids:
        description: A list of group ids to search for.
        required: false
    vpc_id:
        description: The vpc to search in.
        required: false
'''


def main():
    argument_spec = ec2_argument_spec()
    argument_spec.update({
        "groups": {
            "type": "list"
        },
        "group_ids": {
            "type": "list"
        },
        "vpc_id": {
            "type": "str"
        }
    })

    module = AnsibleModule(
        argument_spec = argument_spec,
        mutually_exclusive=["groups", "group_ids"])

    region = module.params.get("region")
    if not region:
        module.fail_json(msg="You must specify a region")

    names = module.params.get("groups")
    ids = module.params.get("group_ids")
    vpc = module.params.get("vpc_id")

    region, ec2_url, aws_connect_kwargs = get_aws_connection_info(module)

    if vpc:
        filters = {'vpc_id': vpc}
    else:
        filters = None

    ec2 = boto.ec2.connect_to_region(region, **aws_connect_kwargs)
    groups = ec2.get_all_security_groups(
        groupnames=None,
        group_ids=ids,
        filters=filters)

    result = []
    for g in groups:
        if g.name in names:
            result.append({
                "name": g.name,
                "id": g.id
            })

    module.exit_json(changed=False, groups=result)

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.ec2 import *

main()
