#!/usr/bin/env bash

read -p "Give environment [hsep_test,hsep_test02,hsep_uat]: " environment
read -p "Give version: " VERSION

PLAYBOOK="hsep_aws_playbook.yml"
USER_FLAG="--user=ubuntu"

TAGS="--tags deploy"
if [[ $* == *--full* ]]; then
    echo "Running the full deployment"
    TAGS=""
fi

if [[ $* == *--elk* ]]; then
    echo "Running only the elk"
    TAGS="--tags elk"
fi

if [[ $* == *--pentaho* ]]; then
    echo "Running only pentaho"
    TAGS="--tags aws_registry_access,docker,pentaho"
fi

if [[ $environment == "hsep_test" ]]; then
    environment="hsep_test/inventory"
fi

if [[ $environment == "hsep_test02" ]]; then
    environment="hsep_test/inventory02"
fi

echo Deploying using $environment

if [[ $environment == "hsep_uat" ]]; then
    USER_FLAG=""
    PLAYBOOK="hsep_playbook.yml"
fi

ansible_command="ansible-playbook -i $environment -e hsep_image_version=$VERSION $PLAYBOOK $TAGS $USER_FLAG"
echo ${ansible_command}

(cd ../ansible && ${ansible_command})
