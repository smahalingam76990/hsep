#!/usr/bin/env bash

read -p "Give version: " version

TAGS="--tags deploy"
if [[ $* == *--full* ]]; then
    echo "Running the full deployment"
    TAGS=""
fi

(cd ../ansible && ansible-playbook -i dsl_data_service -e dsl_data_service_version=$version dsl_data_service_playbook.yml $TAGS --user=ubuntu)
