#!/bin/bash

ver=2.3

docker build -t registry.hflexenergy.com/jenkins_server:$ver .
docker push registry.hflexenergy.com/jenkins_server:$ver
