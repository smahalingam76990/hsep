.. _docker:

Docker
======

Docker is an open-source project that automates the deployment of Linux applications inside software containers.
Docker containers wrap up a piece of software in a complete filesystem that contains everything it needs to run: code, runtime, system tools, system libraries – anything you can install on a server. This guarantees that it will always run the same, regardless of the environment it is running in.

We use docker heavily since it simplifies the orchestration on both local development and server based environments.
The benefit of using docker containers is that we can package the application with all the required packages and files (including the OS itself) and store and deploy that.
In that way the servers need only to have the **docker daemon** running on a linux OS and **nothing else**.

Every project has a ``docker`` directory that contains the relative docker files consisting of:

- Dockerfiles
- docker compose files
- entrypoint and other scripts
- environment variables files

For more information about docker please read the official `documentation`_

.. _documentation: https://docs.docker.com/


Docker Compose
--------------

Docker compose is a tool that helps orchestrate the build and running of the containers on a project.
It splits the application on services

It uses a ``docker-compose.yml`` file where all the services are defined along with instructions on how to build or fetch any docker images and how to run them.

A project can be dependent on docker images that can be built from files that exist on the project codebase or by fetching pre build images from a docker registry.
Inside ``docker-compose.yml`` are defined the dependencies between the several services of the project.

For example, if a rest_api service needs to be started tha depends on a postgres database to be running, a docker-compose command like the following can be invoked.
.. code-block:: console

   $ docker-compose up rest_api

Then docker-compose will follow the instructions ``docker-compose.yml`` in order to build any required images and then start all the required containers in the correct order with the correct parameters.


Deploying docker containers
---------------------------

On test, uat or production environments, we want to use pre-built images since they are packaging everything is needed for the application to run.
The pre-built images are the ones that have passed automated and manual QA and is almost sure that they will behave the same wherever they are deployed.
They remove the need for more complicated procedures that involve the checkout of code and installing multiple different packages, setting up users or directories etc

The pre-built images are built by :ref:`jenkins` and can be pulled and run by :ref:`ansible` scripts given a specific version number.

