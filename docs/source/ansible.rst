.. _ansible:

Ansible
=======

Ansible is a popular orchestration tool written in python.
It has good support for AWS and docker containers and is very easily extensible with modules written in python.

For more information about ansible please read the official `documentation`_

.. _documentation: http://docs.ansible.com/ansible/index.html


Ansible Codebase
----------------

Under ``ansible`` directory are located all the ansible related files (roles, group vars, playbooks etc)


Playbooks
---------

There are playbooks for deploying and configuring:

- HSEP environments
- Jenkins master and slave machines
- Data service
- Data science lab launcher and workers
- Users across machines
- AWS security groups
- Docker registry repos on AWS

Using the above playbooks someone can create virual machines and provision them completely from scratch.
Ansible will request the creation of machines on AWS and then will make sure all the required software is installed and configured along with access to specific users

.. note:: In case of non AWS cloud providers (T-Systems) the scripts can only configure existing machines.


Deploy Scripts
--------------

In order to make easier and hassle free, less error prone deployments, there are some deploy scripts under ``deploy_scripts`` directory
Those scripts will ask for input and will construct and run the appropriate ansible command.

For example deploying hsep with ``deploy_hsep.sh`` will ask for the environment you wish to deploy and the version

.. code-block:: console

   $ ./deploy_hsep.sh
   Give environment [hsep_test,hsep_test02,hsep_uat]: hsep_test
   Give version: develop_234
   Deploying using hsep_test/inventory
   ansible-playbook -i hsep_test/inventory -e hsep_image_version=develop_234 hsep_aws_playbook.yml --tags deploy --user=ubuntu

The script will make sure it uses the correct inventory and group vars and playbook for the specific environment.
Notice that it prints the command before it runs it so you can copy paste and run it again directly if it's required.

.. note:: The user that runs an AWS deployment, should have the root aws keys installed on their ssh keys.
   This is already set up on the ansible launcher machine, to use it follow:
   - ssh into the ansible launcher machine (can use aws-fuzzy)
   - switch to ubuntu user using ``sudo su ubuntu``
   - ``cd ~/infrastructure/deploy_scripts/``
   - normal deployment ``deploy_hsep.sh``
   - full deployment ``./deploy_hsep.sh --full``
   For non AWS deployments, the user needs to have password-less sudo ssh access on the target machines.


Inventories
-----------

Ansible is using inventories in order to define which machines belong to which host groups and then plays can run against specific host groups
In that way playbooks can be organised to deploy specific services (docker containers) on specific machines.
For hsep on :ref:`aws` we populate the inventory on the fly but for :ref:`tsystems` the inventory is manually hardcoded since the machines are manually created

Currently, on both cases, the host groups on the inventories have the same host for every host group (thats clear if someone looks on the inventory for hsep_uat)
That means that all services will end up on the same host. The only exception is the pentaho_integration service.
On production there will be the need for scaling up so the inventory should be constructed in a way that different host is used for different groups (or multiple hosts in a group)

SSH Config
----------

(Only for non aws environments like T-Systems)

An SSH config example template is located on ``ssh_config/config`` file. This file should be updated whenever new machines are created or IPs are changing
The user that wants to access a machine needs to update their local ssh config file accordingly.
Also needs to make sure that a user exists on the target machine with authorised keys and password-less sudo.

