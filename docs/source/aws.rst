.. _aws:

AWS
===

AWS services offers a suite of cloud-computing services that make up an on-demand computing platform.
For data protection reasons all the services we are using are located on the Frankfurt region (eu-central-1).

The HSEP services that are hosted on AWS are

- HSEP rest api and web ui for test environments
- Jenkins Master and slaves
- Data science lab (Data service, DSL launcher, DSL workers)

For more information about AWS please read the official `documentation`_

To access the aws console please follow that link `aws_console`_

.. _documentation: https://aws.amazon.com/documentation/
.. _aws_console: https://678638262724.signin.aws.amazon.com/console


AWS Services
------------

The main services that we are using on AWS are the following

- EC2 Cloud computing
- RDS
- Route53
- S3
- Container service


AWS Resources
-------------

In order to reduce AWS cost, we are trying to use the correct type of machines to create.

**Jenkins slaves**

We chose to use for jenkins slaves CPU burstable machines (cheaper machines) since they are not CPU intensive for very long times.
For simulator nightly builds we have created a non-burstable slave because nightly builds are running for long period of times. In this case we want to avoid poor performance because of cpu credits exhaustion.

Also we are manually shutting down slaves that are not doing anything and we bring them up again when the queue for jobs waiting to run is becoming full.

**DSL Workers**

The DSL Workers, since they are running for long time and there is a risk of cpu credits exhaustion, we chose to use non-burstable machines.
Also since we run the whole stack of services in each worker we chose to use machines with 8GB of RAM

**HSEP server**

Since we run the whole stack of HSEP services on one machine we chose to use machines with 8GB of RAM but a burstable CPU is more than enough.

Security Groups
---------------

Currently we control access on the services by using firewall rules on amazon aws security groups.
A detailed and **updated** list of the security groups can be found on security_groups_playbook.yml
We prefer not to list them here since they are changing often.
