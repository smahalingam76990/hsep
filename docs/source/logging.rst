.. _logging:

Centralised Logging
===================

All of the applications are using logging statements to log activity.
Since the applications are using multiple services that can be located in different machines, we put in place a centralised logging system based on `ELK stack`_

.. _ELK stack: https://www.elastic.co/webinars/introduction-elk-stack

The `ELK stack`_ consists of the following technologies:

- Elasticsearch for storing the logs
- logstash for getting logs from different sources and sending them to Elasticsearch
- Kibana for visualisation


Every python application we have is configured by a relatively common logging configuration that can be found on hsep_utils package.
This is configured to send any log statements to a rsyslog daemon that then makes sure to write the logs on files.
The reason of using this approach and not writing directly in files has to do with the fact that is not safe to write on the same file from multiple processes.

A logstash process is running next to each syslog daemon in order to read the log files and send the logs into elasticsearch
Every log is already in json format in order to simplify the work for logstash by not having to parse the messages.


Deploying ELK
-------------

The ELK stack can be used on both local development and server-deployed environments.
As always, for local development we orchestrate everything with :ref:`docker` and for servers we do it with :ref:`ansible`
So please refer to the appropriate sections to find more information


Logs vs Audit Logs
------------------

We have 2 different types of logs

- Normal logging statements with different log level
- Audit logs

Kibana by default is looking for logs on indexes in the logstash-* pattern
New indexes are created every day, for example logstash-2016.12.23 or auditing-2016.12.23

In order to be able to see the audit logs since kibana creates an index for logstash-* pattern only, the user should use the menus to add also the auditing-* index as well
Then during discovery someone can choose which index to search in.

