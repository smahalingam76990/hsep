.. HSEP Infrastructure documentation master file, created by
   sphinx-quickstart on Tue Dec 20 12:33:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HSEP Infrastructure's documentation!
===============================================

This documentation describes the infrastructure for HSEP services.
It contains information for deploying the main product, running simulations and about the development tools.

Software Distribution
---------------------
The software is packaged and distributed by using :ref:`docker` images
The packages are built by :ref:`jenkins` and stored on a docker registry hosted on :ref:`aws`

Environments
------------
Currently we have a test environment hosted on :ref:`aws` and a uat environment hosted on :ref:`tsystems`

Orchestration
-------------
Deployments and creation of machines and environments for all services are almost fully automated using :ref:`ansible`

Test automation
---------------
Almost every piece of software developed is accompanied by automated tests.
Before a :ref:`docker` image is pushed on the docker registry, all the automated tests are executed to prove that main functionality is not broken
:ref:`jenkins` takes care of the buildig and QA of the images

Monitoring and logging
----------------------
We have setup a :ref:`logging` system based on ELK that makes sure all logs are collected and stored on a centralised place to be accessed.
We have basic monitoring only on :ref:`aws`, provided by aws itself


Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ansible
   docker
   jenkins
   pentaho
   aws
   tsystems
   logging

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
