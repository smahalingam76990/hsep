.. _pentaho:

Pentaho
=======

Pentaho is used as a scheduler that fetches csv files from ftp sites that provide paid services for historical market data.
Currently we have access for EPEX and PriceIT historical data.

Pentaho jobs consists of steps that are fetching csv files from ftp sites, applies a transformation function that creates json files from csv and finally using the hsep rest api to PUT the data on the HSEP sytem for specific environment.

Currently we have jobs for:

- DAHA
- IQA
- HPFC
- QPFC


Pentaho servers
---------------

Pentaho is running on Ubuntu Desktop virtual machines on AWS for the test environment and on T-Systems for the UAT environment
Since pentaho relies on a UI, the servers are running an X server with a simple GUI.
The user needs to use a VNC viwer to connect and interact with pentaho UI. The more straight forward way to do it is by using the ssh tunnel option on the vnc viewer by putting the IP or domain name of the machine, the username and ssh key file. Then as the main VNC host can be used localhost.

.. note:: In order to see how to find the ssh connection details for a machine, please refer to :ref:`aws` and :ref:`tsystems` sections.

Pentaho deployment
------------------

The HSEP ansible deployment scripts are including the pentaho machine provision and pentaho deployment. Since ansible runs through SSH without using a GUI on the server, instead of running the container, it creates a script on user's ubuntu home directory `run_pentaho.sh`. The user should then use VNC to connect and run the script in order to finish the deployment and bring the Pentaho UI up.
After Pentaho UI is running the user should open the desired jobs and run them in scheduled mode.

Pentaho UI
----------

After successfully connected with VNC and given a previously successful deployment, the pentaho UI should be there up and running (see previous section if is not). The user can choose the job he wants and run it with the appropriate parameters (make sure it removes any scheduled mode first). 
Usually users want to put a range of dates for the job to run in order to fill gaps in the data.
