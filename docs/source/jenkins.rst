.. _jenkins:

Jenkins
=======

Jenkins is a continuous integration and continuous delivery application.
We are using Jenkins to build and test our projects continuously making it easier for developers to integrate changes to the project and to obtain a fresh build.

Jenkins has two main parts. The jenkins master and one or more jenkins slaves.
 Jenkins master has all the configuration and the UI for users to access it
 Jenkins slaves are triggered by master and run the jobs and produce results that are going back to master to be stored as artifacts

For more information about Jenkins please read the official `documentation`_

To access the current jenkins master UI please follow that link `jenkins_ui`_

.. _documentation: https://jenkins.io/doc/
.. _jenkins_ui: https://jenkins.hflexenergy.com/


Jenkins Jobs
------------

The Jobs that are configured on Jenkins master are the following:

- DSL develop build
- DSL feature build
- DSL master build
- HSEP develop build
- HSEP feature build
- HSEP master build
- Simulator Nightly
- Heat Simulator Nightly

The above ``build`` jobs are configured to be triggered from github whenever there is some code changes. Usually the steps are

- Git checkout code
- Build docker images
- Run tests (unit and integration tests)
- Tag and push images on docker registry
- Artifact test reports

The above ``Nightly`` jobs are configured run periodically every day. The steps are:

- Git checkout latest develop code
- Build docker images
- Run simulation with the pre-configured default parameters
- Extract and artifact simulation results


Jenkins job builder
-------------------

The configuration for the jobs is stored and maintained as a yml file on every project's git repo, usually under jenkins directory along with any relevant scripts.
Under `jenkins/jobs` directory the jobs.yml is located that describes all the job's details that need to be configured on jenkins for the specific project.
There is also a script that someone can run in order to apply that information against a jenkins master.
The connection details for accessing jenkins master are located on conf.ini
The requirements file needs to be installed in order to get the jenkins-job-builder executable.

Follows an example of run of the update_jobs script on hsep project

.. code-block:: console

    $ ./update_jenkins.sh
    INFO:root:Updating jobs in ['jobs.yml'] ([])
    INFO:jenkins_jobs.builder:Number of jobs generated:  5
    INFO:jenkins_jobs.builder:Reconfiguring jenkins job heat_simulator
    INFO:jenkins_jobs.builder:Reconfiguring jenkins job hsep_develop
    INFO:jenkins_jobs.builder:Reconfiguring jenkins job hsep_feature
    INFO:jenkins_jobs.builder:Reconfiguring jenkins job hsep_master
    INFO:jenkins_jobs.builder:Reconfiguring jenkins job simulator
    INFO:root:Number of jobs updated: 5

For more information about jenkins-job-builder look at the official `job_builder_documentation`_

.. _job_builder_documentation: http://docs.openstack.org/infra/jenkins-job-builder/


Master branch vs develop branch vs feature branch
-------------------------------------------------
We are using a different jenkins job to build and test code that is on github pull requests and code that is on more stable branches like develop and master.
Theoretically the more stable develop and master branches should always have passing tests and successful builds.
When a developer creates a new pull request on github, the the feature job will be triggered and when it finishes will report back to github the status of the build (successful or not).
A successful build will result on images to be tagged and pushed by having the type of branch as prefix and the number of build as suffix.
For example a develop branch build will result on images tagged as develop_123, where 123 is an example of build number.

By logging on to :ref:`aws` console and going to EC2 Container Service you will be able to go through the different registry repos and see which images with which tags are available.


Jenkins master and slaves deployment
------------------------------------

On the ``ansible`` directory are located ansible playbooks that can provision a Jenkins master and multiple slave machines
for more information about using the ansible playbooks look at the :ref:`ansible` section.


