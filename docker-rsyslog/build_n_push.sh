#!/bin/bash

set -e

if [ -z "$1" ]
  then
    echo "ERROR: No version number supplied" 1>&2
    exit 1
fi

ver=$1

docker build -t 678638262724.dkr.ecr.eu-central-1.amazonaws.com/rsyslog:$ver .

docker push 678638262724.dkr.ecr.eu-central-1.amazonaws.com/rsyslog:$ver
