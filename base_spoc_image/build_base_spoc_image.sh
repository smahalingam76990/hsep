#!/bin/bash

set -e

if [ -z "$1" ]
  then
    echo "ERROR: No version number supplied" 1>&2
    exit 1
fi

ver=$1

REGISTRY=678638262724.dkr.ecr.eu-central-1.amazonaws.com

docker build -t ${REGISTRY}/base_spoc:$ver .
docker tag -f ${REGISTRY}/base_spoc:$ver ${REGISTRY}/base_spoc:latest

docker push ${REGISTRY}/base_spoc:$ver
docker push ${REGISTRY}/base_spoc:latest
